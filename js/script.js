var longitude;
var latitude;
var tempC;
var tempF;
var minC;
var minF;
var maxC;
var maxF;
var inFarenheit = true;

function getJSON(latitude, longitude) {
  req=new XMLHttpRequest();
  req.open("GET",'https://fcc-weather-api.glitch.me/api/current?lon='+longitude+'&lat='+latitude,true);
  req.send();
  req.onload=function(){
    json=JSON.parse(req.responseText);
    console.log(json);
    loadPage(json);
  };
}

function convertToF(celsius) {
  return Math.floor(celsius * (9/5) + 32);
}

function loadPage(json) {
  document.getElementById("loading").style.display = "none";
  loadElement("area", json["name"]);
  loadElement("weather", json["weather"][0]["main"]);
  loadElement("description", json["weather"][0]["description"]);
  tempC = Math.floor(parseFloat(json["main"]["temp"]));
  tempF = convertToF(tempC);
  loadElement("temperature", tempF + "°F");
  minC = json["main"]["temp_min"];
  maxC = json["main"]["temp_max"];
  minF = convertToF(minC);
  maxF = convertToF(maxC);
  loadElement("min", "Min: " + minF + "°F");
  loadElement("max", "Max: " + maxF + "°F");
  if (json["weather"][0]["icon"]) {
    var icon = document.createElement("img");
    icon.src = json["weather"][0]["icon"];
    icon.alt = "icon of current weather";
    document.getElementById("icon").appendChild(icon);
  }
  document.getElementById("changeUnits").style.display = "inline-block";
}

function loadElement(id, data) {
  document.getElementById(id).innerHTML = data
}

if (navigator.geolocation){
  navigator.geolocation.getCurrentPosition(function(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    getJSON(latitude, longitude);
  });
}
window.onload = function() {
  document.getElementById("changeUnits").onclick = function() {
    if (inFarenheit) {
      loadElement("temperature", tempC + "°C");
      loadElement("min", "Min: " + minC + "°C");
      loadElement("max", "Max: " + maxC + "°C");
      loadElement("changeUnits", "Get Farenheit");
      inFarenheit = false;
    }
    else {
      loadElement("temperature", tempF + "°F");
      loadElement("min", "Min: " + minF + "°F");
      loadElement("max", "Max: " + maxF + "°F");
      loadElement("changeUnits", "Get Celsius");
      inFarenheit = true;
    }
  }
}
